
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="./">todolist</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="./?action=<?php if(!isset($_SESSION['login'])):?> connexion <?php else : ?>deconnexion<?php endif;?>" ><?php if(!isset($_SESSION['login'])):?> Connexion <?php else : ?>Deconnexion<?php endif;?></a>
                <button class="navbar-toggler" type="button" name="connexion" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </li>
        </ul>
    </div>
    <div>
        <?php if (isset($_SESSION['login'])) : ?><?= $_SESSION['login'] ?><?php endif;?>
    </div>
</nav>
