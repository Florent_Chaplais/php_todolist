<?php
global $vues;
require_once($vues['header.php']);
?>
<body>

<div class="col-4">
    <?php

    if (!empty($list)):
        foreach ($list as $lt):
            ?>
            <div>
                <form action="./?action=insertList" method="post">
                    <input name="listName" type="text" formmethod="post">
                    <input type="submit" name="insertList" value="ajouter">
                </form>
            </div>
            <div class="card">
                <div class="card-header">
                    <?= $lt->getName() ?>
                    <form action="./?action=deleteList" method="post">
                        <input type="submit" name="deleteList" value="delete">
                    </form>
                </div>
                <?php
                foreach ($lt->getTasks() as $task):
                    ?>
                    <div class="card-body">

                        <form action="./?action=updateTask" method="post">
                            <input type="checkbox" formmethod="post"
                                   name="check_list"<?php if ($task->getStatus() == 1): ?> value="1" <?php else: ?> value="0"<?php endif; ?> <?php if ($task->getStatus() == 1): ?> checked <?php endif; ?>>
                            <label> <?= $task->getTitle(); ?> </label>
                        </form>
                        <form action="./?action=deleteTask" method="post">
                            <input type="submit" name="deleteTask" value="delete">
                        </form>
                    </div>
                <?php
                endforeach;
                ?>
                <div class="card-body">
                    <form action="./?action=insertTask" method="post">
                        <input type="checkbox" name="status">
                        <input name="taskName" type="text">

                        <input type="submit" name="submit">
                    </form>
                </div>
                <?php
                ?>
            </div>

        <?php
        endforeach;
    else:
        ?>
        <span>
</span>
    <?php
    endif;
    ?> </div>
</body>
<?php
require_once($vues['footer.php']);
?>
