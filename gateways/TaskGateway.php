<?php
class TaskGateway
{
    private $con;

    public function __construct($con)
    {
        $this->con=$con;

    }

    public function selectAll(int $id)
    {
        $query="SELECT * FROM task WHERE idlist = :id";
        $res=$this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        if(!$res){
            throw new Exception("Erreur du select");
        }
        $res=$this->con->getResults();

        return $res;
    }

    public static function deleteTask($id, $con)
    {
        $query = 'Delete from task where id=:id';
        $res = $con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        if (!$res) {
            throw new Exception("erreur suppression");
        }
    }

    public static function insertTask($idlist, $status, $title, $con)
    {
        $query = 'Insert into task VALUES(:idlist,:status,:title)';
        $res = $con->executeQuery($query, array(
            ':idlist' => array($idlist,PDO::PARAM_INT),
            ':title' => array($title,PDO::PARAM_STR),
            ':status' => array($status,PDO::PARAM_INT)
        ));
        if (!$res) {
            throw new Exception("erreur suppression");
        }
    }

    public static function updateTask($id, $status, $con)
    {
        $query="UPDATE task SET status = :status WHERE id = :id;";
        $res=$con->executeQuery($query,array(
            ':id' => array($id,PDO::PARAM_INT),
            ':status' => array($status,PDO::PARAM_INT)
        ));
        if(!$res) {
            throw new Exception("L'insertion a échoué!");
        }

    }
}
