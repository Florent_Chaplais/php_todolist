<?php


class GatewayUser extends TaskGateway
{
    private $connection;

    function __construct(Connection $connection)
    {
        $this->connection=$connection;
    }

    public function selectUser(string $user)
    {
        try {
            $query = 'SELECT pwd, id FROM user WHERE name =:user;';
            $this->connection->executeQuery($query, array(':user'=>array($user,PDO::PARAM_STR)));
            return $this->connection->getResults();
            }catch (PDOException $PDOException){
            echo $PDOException->getMessage();
        }
    }

}