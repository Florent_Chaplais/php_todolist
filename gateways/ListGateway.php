<?php


class ListGateway
{
    private $con;

    public function __construct($con)
    {
        $this->con=$con;

    }

    public function selectAll()
    {
        $query="SELECT * FROM list";
        $res=$this->con->executeQuery($query);
        if(!$res){
            throw new Exception("Erreur du select");
        }
        $res=$this->con->getResults();

        return $res;
    }

    public function selectAllPrivate($id)
    {
        $query="SELECT * FROM list where iduser = :id";
        $res=$this->con->executeQuery($query,array(':id' => array($id, PDO::PARAM_INT)));
        if(!$res){
            throw new Exception("Erreur du select");
        }
        $res=$this->con->getResults();
        return $res;
    }

    public function deleteList($id, $con)
    {
        $query = 'Delete from list where id=:id';
        $res = $con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        if (!$res) {
            throw new Exception("erreur suppression");
        }
    }

    public function insertList($iduser, $name, $con)
    {
        $query = 'Insert into list VALUES(:id,:iduser,:name)';
        $res = $con->executeQuery($query, array(
            ':iduser' => array($iduser,PDO::PARAM_INT),
            ':name' => array($name,PDO::PARAM_STR),
        ));
        if (!$res) {
            throw new Exception("erreur suppression");
        }
    }

}