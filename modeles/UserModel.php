<?php


class UserModel
{
    public function login(string $name, string $pwd)
    {
        $userPwd = $this->getUserPwd($name);
        if (empty($userPwd)) {
            throw new Exception('User inconnu');
        }

        if (password_verify($pwd, $userPwd[0]['pwd'])) {
            $_SESSION['login'] = $name;
            $_SESSION['id'] = $userPwd[0]['id'];
            $_SESSION['role'] = 'user';
            header('location:./');
            return new User($name, $pwd);
        }
        header('location:./?action=connexion');

    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION);
        $_SESSION = array();
    }

    private function getUserPwd(string $user)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        $GatewayUser = new GatewayUser($con);
        return $this->user = $GatewayUser->selectUser($user);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

}