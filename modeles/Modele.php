<?php

class Modele
{

    public static function insertList($id,$name)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        $ListGateway = new ListGateway($con);
        $ListGateway->insertList($id,$name,$con);
    }

    public static function insertTask($id,$status,$name)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        $TaskGateway = new TaskGateway($con);
        $TaskGateway->insertTask($id,$status,$name,$con);
    }

    public function findAllPublic()
    {
        global $base, $login, $mdp;
        $taskList = array();
        $con = new Connection($base, $login, $mdp);
        $ListGateway = new ListGateway($con);
        try {
            $list = $ListGateway->selectAll();
        } catch (Exception $e) {
            $e->getMessage();
        }
        foreach ($list as $lt) {
            if ($lt['iduser'] == 0) {
                $tasks = array();
                $TaskGateway = new TaskGateway($con);
                try {
                    $task = $TaskGateway->selectAll($lt['id']);
                } catch (Exception $e) {
                    $e->getMessage();
                }
                foreach ($task as $tk) {
                    array_push($tasks, new Task($tk['id'], $tk['idlist'], $tk['title'], $tk['status']));
                }
                array_push($taskList, new ListTask($lt['id'], $lt['iduser'], $lt['name'], $tasks));
            }
        }
        return $taskList;
    }

    public function findAllPrivate($id)
    {
        global $base, $login, $mdp;
        $taskList = array();
        $con = new Connection($base, $login, $mdp);
        $ListGateway = new ListGateway($con);
        try {
            $list = $ListGateway->selectAllPrivate();
        } catch (Exception $e) {
            $e->getMessage();
        }
        foreach ($list as $lt) {
            $tasks = array();
            $TaskGateway = new TaskGateway($con);
            try {
                $task = $TaskGateway->selectAll($lt['id']);
            } catch (Exception $e) {
                $e->getMessage();
            }
            foreach ($task as $tk) {
                echo $tk['title'];
                array_push($tasks, new Task($tk['id'], $tk['idlist'], $tk['title'], $tk['status']));
            }
            array_push($taskList, new ListTask($lt['id'], $lt['iduser'], $lt['name'], $tasks));

        }
        return $taskList;
    }

    public static function deleteTask($id)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        TaskGateway::deleteTask($id, $con);
    }

    public static function deleteList($id)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        ListGateway::deleteList($id, $con);
    }

    public static function updateTask($id, $status)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        TaskGateway::updateTask($id, $status, $con);
    }

    public static function updateList($id, $name, $status)
    {
        global $base, $login, $mdp;
        $con = new Connection($base, $login, $mdp);
        TaskGateway::insertTask($id, $status, $name, $con);
    }
}