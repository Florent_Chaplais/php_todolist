<?php


class userControl
{
    function __construct(string $action)
    {
        switch ($action){
            case NULL:
            case 'affichList':
                $this->affichList();
                break;
            case "login":
                $this->login();
                break;
            case 'deconnexion':
                $this->deconnexion();
                break;
        }
    }
    public function mainView()
    {
        require_once('vues/vueGuest.php');
    }
    public function addList(string $name, string $tasks)
    {
        if(empty($name))
        {
            throw new Exception("erreur de saisie");
        }
        GatewayUser::insertList($name, $tasks);
    }
    public function delNews(TaskList $list)
    {
        GatewayUser::deleteList($list->getId());
    }

    public function affichList(){
        global $vues;
        $modele = new Modele();
        $list = $modele->findAllPublic();
        $list = array_merge($list, $modele->findAllPrivate($_SESSION['id']));
        var_dump($list);
        require_once($vues['vueGuest.php']);
    }

    public function login(){
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $pseudo = Validation::purifString($_POST['username']);
            $mdp = Validation::purifString($_POST['password']);
            $mAdmin = new UserModel();
            try {
                $mAdmin->login($pseudo, $mdp);
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
    }

    private function deconnexion()
    {
        global $vues;
        session_unset();
        session_destroy();
        setcookie('noUser','',-3600,'/','localhost',false,true);
        $_SESSION = array();
        $_SESSION['role']='public';
        require_once($vues['vueGuest.php']);
    }
}