<?php

class publicControl {

    function __construct($action)
    {
        global $rep, $vues;
        $dVueErreur = array ();

        switch ($action) {
            case 'affichList':
            case null:
                $this->affichList();
                break;
            case "connexion":
                require_once($rep.$vues['vueConnexion']);
                break;
            case 'deleteTask':
                $this->deleteTask();
                break;
            case 'deleteList':
                $this->deleteList();
                break;
            case 'updateTask':
                $this->updateTask();
                break;
            case 'insertList':
                $this->insertList();
                break;
            case 'insertTask':
                $this->insertTask();
                break;
            default:
                $dVueErreur[] = "Erreur d'appel php";
                break;
        }
    }

    public function affichList(){
        global $vues;
        $modele = new Modele();
        $list = $modele->findAllPublic();
        require_once($vues['vueGuest.php']);
    }

    private function deleteTask()
    {
        Modele::deleteTask($_POST['deleteTask']->getId());
    }

    private function deleteList()
    {
        Modele::deleteList($_POST['deleteList']->getId());
    }

    private function updateTask()
    {
        Modele::updateTask($_POST['check_list'],$_POST['updateTask']->getId());
    }

    private function insertList()
    {
        Modele::insertList($_SESSION['id'],$_POST['listName']);
    }

    private function insertTask()
    {
        Modele::insertTask($_SESSION['id'],$_POST['checkbox'],$_POST['taskName']);
    }

}