<?php


class FrontController
{


    function __construct()
    {
        $actionPublic = ['connexion','deleteList','deleteTask','updateTask','insertList','insertTask'];
        $actionUser = ['login', 'deconnexion'];
        $action = null;
        $dVueErreur = array ();
        try {
            if (isset($_REQUEST['action'])) {
                $action = Validation::purifString($_REQUEST['action']);
            } else {
                $action = null;
            }
            if ($action == 'affichList'){
                if ($_SESSION['role'] == 'public'){
                    new PublicControl($action);
                }
                else if ($_SESSION['role'] == 'user'){
                    new UserControl($action);
                }
            }
            if (in_array($action,$actionPublic)|| $action == null){
                new PublicControl($action);
            }

            if (in_array($action,$actionUser)){
                new UserControl($action);
            }

        } catch (PDOException $e) {
            $dVueErreur[] = "Erreur de connexion à la BDD";
        } catch (Exception $e) {
            $dVueErreur[] = $e->getMessage();
        }
    }
}