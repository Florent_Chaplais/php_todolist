<?php

class Validation {

static function val_action($action) {

if (!isset($action)) {
throw new Exception('pas d\'action');
    //on pourrait aussi utiliser
//$action = $_GET['action'] ?? 'no';
    // This is equivalent to:
    //$action =  if (isset($_GET['action'])) $action=$_GET['action']  else $action='no';
}
}

    static function val_form(string &$nom, string &$age, array &$dVueEreur) {

        if (!isset($nom)||$nom=="") {
            $dVueEreur[] =	"pas de nom";
            $nom="";
        }

        if ($nom != filter_var($nom, FILTER_SANITIZE_STRING))
        {
            $dVueEreur[] =	"tentative d'injection de code (attaque sécurité)";
            $nom="";

        }

        if (!isset($age)||$age==""||!filter_var($age, FILTER_VALIDATE_INT)) {
            $dVueEreur[] =	"pas d'age ";
            $age=0;
        }

    }

    static function purifString(string $text){
        $text=filter_var($text, FILTER_SANITIZE_URL);
        $text=filter_var($text,FILTER_SANITIZE_STRING);
        return $text;

    }

    static function purifInt(int $int){
        return filter_var($int,FILTER_SANITIZE_INT);
    }

    static function validation_admin(string &$pseudo, string &$mdp, array &$VueErreurCoAdmin){

        if (!isset($pseudo)||$pseudo=="") {
            $VueErreurCoAdmin[] =	"Veuillez saisir un nom d'utilisateur";
            $pseudo="";
        }

        if ($pseudo != filter_var($pseudo, FILTER_SANITIZE_STRING))
        {
            $VueErreurCoAdmin[] =	"tentative d'injection de code (attaque sécurité)";
            $pseudo="";
        }


        if (!isset($mdp)||$mdp=="") {
            $VueErreurCoAdmin[] =	"Veuillez saisir un mot de passe";
            $mdp="";
        }

        if ($mdp != filter_var($mdp, FILTER_SANITIZE_STRING))
        {
            $VueErreurCoAdmin[] =	"tentative d'injection de code (attaque sécurité)";
            $mdp="";

        }

    }

}
?>

        