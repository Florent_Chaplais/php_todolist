<?php


class User
{
    private $user;
    private $pwd;

    function __construct($user, $pwd)
    {
        $this->user = $user;
        $this->pwd = $pwd;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @param mixed $pwd
     */
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
    }

    private function createTable()
    {

    }

}