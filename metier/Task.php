<?php


class Task
{
    private $id;
    private $idlist;
    private $title;
    private $status;

    /**
     * Task constructor.
     * @param $id
     * @param $idlist
     * @param $title
     * @param $status
     */
    public function __construct($id, $idlist, $title, $status)
    {
        $this->id = $id;
        $this->idlist = $idlist;
        $this->title = $title;
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdlist()
    {
        return $this->idlist;
    }

    /**
     * @param mixed $idlist
     */
    public function setIdlist($idlist): void
    {
        $this->idlist = $idlist;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

}