<?php


class ListTask
{
private $id;
private $iduser;
private $name;
private $tasks;

    /**
     * ListTask constructor.
     * @param $id
     * @param $iduser
     * @param $name
     * @param $tasks
     */
    public function __construct($id, $iduser, $name, $tasks)
    {
        $this->id = $id;
        $this->iduser = $iduser;
        $this->name = $name;
        $this->tasks = $tasks;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param mixed $iduser
     */
    public function setIduser($iduser): void
    {
        $this->iduser = $iduser;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks): void
    {
        $this->tasks = $tasks;
    }



}